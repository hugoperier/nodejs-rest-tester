import config from '../config.json';

import AuthTest from './tests/AuthTest'
import AdminTest from './tests/AdminTest';
import TicketTest from './tests/TicketTest';
import SiteTest from './tests/SiteTest';


(async () => {
    const authTest = new AuthTest(config)
    await authTest.start();

    const ticketTest = new TicketTest(config)
    await ticketTest.start();

    const siteTest = new SiteTest(config)
    await siteTest.start()

    const adminTest = new AdminTest(config)
    await adminTest.start();

    console.log('Finished.')
})()