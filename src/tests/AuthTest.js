import IApi from "./IApi";
import { getLine } from "../utils/getLine";

export default class AuthTest extends IApi {
  constructor(config) {
    super(config);
    console.log('----- Auth Tests -----')
  }

  /*
   * This test allow us to create an user, the informations must not be already used in the
   * database, we will then use this account for the following tests.
   */
  async RegisterSuccessTest() {
    const res = await this.axios.post("/auth/register", {
      firstName: "Hugo",
      lastName: "PERIER",
      email: this.config.defaultUser.email,
      birthday: "20-05-1998",
      password: this.config.defaultUser.password,
    });
    if (res && res.status === 200 && res.data.success)
      {
        this.config.defaultUser.id = res.data.data.id;
        return true
      }
  }

  /*
   * For this test we use the exact same information as the previous request.
   * This test should fail because the account is already registered
   */
  async RegisterFailTest() {
    const res = await this.axios.post("/auth/register", {
      firstName: "Hugo",
      lastName: "PERIER",
      email: this.config.defaultUser.email,
      birthday: "20-05-1998",
      password: this.config.defaultUser.password,
    });
    return res && res.status === 200 && !res.data.success;
  }

  /*
   * This test should faild because we are using a bad password
   */
  async LoginWithBadCredentialsTest() {
    const res = await this.axios.post("/auth/login", {
      email: this.config.defaultUser.email,
      password: "ABadPassword",
    });
    return res && res.status === 200 && !res.data.success;
  }

  /*
   * This test allow us to try the connection and getting the jwt for futures operations
   */
  async LoginSuccessTest() {
    return await this.LoginDefaultUser()
  }

  /*
   * Theses 3 next tests allow us to verify the user's account thanks to the verification code getted by email
   */

  async VerifyNoTokenTest() {
    const res = await this.axios.get(`/auth/verify`);
    return res && !res.data.success;
  }

  async VerifyBadTokenTest() {
    const res = await this.axios.get(
      `/auth/verify?verificationToken=xxxxxxxxxxxxxx`
    );
    return res &&  !res.data.success;
  }

  async VerifySuccessTest() {
    const token = await getLine("\nCopy and paste the verification token");
    const res = await this.axios.get(`/auth/verify?verificationToken=${token}`);
    return res && res.status === 200 && res.data.success;
  }

  /*
  * Lets verify the password reset 
  */
  async sendResetMailPasswordTest() {
    const res = await this.axios.get(`/auth/resetPassword?email=${this.config.defaultUser.email}`)
    return (res && res.status === 200 && res.data.success)
  }

  async resetPasswordBadTokenTest() {
    const res = await this.axios.post(`/auth/changePassword`, {
      password: 'damoria',
      token: 'fqefeggeg'
    });
    return res && !res.data.success;
  }

  async resetPasswordTest() {
    const token = await getLine("\nCopy and paste the password token");
    await this.axios.post(`/auth/changePassword`, {
      password: 'damoria74',
      token: token
    });
    const res = await this.axios.post("/auth/login", {
      email: "hugo.perier@epitech.eu",
      password: "damoria74",
    });
    return res && res.status === 200 && res.data.success;
  }
}
