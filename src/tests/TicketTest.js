import IApi from "./IApi";

export default class TicketTest extends IApi {
  constructor(config) {
    console.log('----- Tickets Tests -----')
    super(config);    
  }

  async start() {
    await this.LoginDefaultUser()
    await super.start()
  }

  async CreateTicketTest() {
    const res = await this.axios.post("/ticket/create", {
      subject: "Help me I cannot suscribe",
      message:
        "today while trying to suscribing, I did encounter an error 500 on the website, do you have explication ?",
    });
    if (res && res.status === 200 && res.data.success) {
      this.config.ticketCreatedId = res.data.data.id;
      return true;
    }
  }

  async GetMyTicketsTest() {
    const res = await this.axios.get("/ticket/myTickets");
    return (
      res && res.status === 200 && res.data.success && res.data.data.length > 0
    );
  }

  async AddMessageTest() {
    const res = await this.axios.post("/ticket/addMessage", {
      ticketId: this.config.ticketCreatedId,
      message: "Oh no okay my bad I was on my own website",
    });
    return res && res.status === 200 && res.data.success;
  }

  async UpdateStatusTest() {
    const res = await this.axios.post("/ticket/update", {
      ticketId: this.config.ticketCreatedId,
      status: "closed",
    });
    return res && res.status === 200 && res.data.success;
  }
}
