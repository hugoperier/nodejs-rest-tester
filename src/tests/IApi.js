let axios = require("axios");
const https = require("https");
import ora from "ora";

export default class IApi {
  constructor(config) {
    this.axios = axios.create({
      baseURL: config.baseUrl,
      timeout: 60000,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    });
    this.config = config;
    if (config.jwt) this.setJwt(config.jwt);
  }

  async start() {
    await this.testAll();
  }

  setJwt(jwt) {
    this.axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
  }

  async LoginDefaultUser() {
    const res = await this.axios.post("/auth/login", {
      email: this.config.defaultUser.email,
      password: this.config.defaultUser.password,
    });
    if (res && res.status === 200 && res.data.success) {
      this.setJwt(res.data.data.token);
      return true;
    }
  }

  async testAll() {
    const methods = this.getAllMethodNames();
    for (const method of methods) {
      const spinner = new ora({
        text: `Testing ${method}`,
      });
      spinner.start();
      try {
        const res = await this[method]();
        if (res) spinner.succeed();
        else spinner.fail();
      } catch (error) {
        spinner.fail();
        console.log("ERROR\n", error);
      }
    }
  }

  getAllMethodNames(filters) {
    let methods = new Set();
    let obj = this;
    while ((obj = Reflect.getPrototypeOf(obj))) {
      let keys = Reflect.ownKeys(obj);
      keys.forEach((k) => methods.add(k));
    }
    const res = Array.from(methods).filter((e) => e.endsWith("Test"));
    if (filters && filters.length > 0) {
      return res.filter((e) => filters.includes(e));
    }
    return res;
  }
}
