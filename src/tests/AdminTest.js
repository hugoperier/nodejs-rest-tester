import IApi from "./IApi";

export default class AdminTest extends IApi {
  constructor(config) {
    super(config);
    console.log('----- Admin Tests -----')
  }

  async ConnectAdminTest() {
    const res = await this.axios.post("/auth/login", {
      email: "mrmusicdu74@gmail.com",
      password: "damoria74",
    });
    if (res && res.status === 200 && res.data.success) {
      this.setJwt(res.data.data.token);
      return true;
    }
    return false;
  }

  async CreateAccountTest() {
    const res = await this.axios.post("/account/create", {
      firstName: "Hugo",
      lastName: "PERIER",
      email: "toto@gmail.eu",
      birthday: "20-05-1998",
      password: "damoria74",
      roleId: 1
    });
    if (res && res.status === 200 && res.data.success && res.data.data.id) {
        this.config.deleteAccountId = res.data.data.id
        return true
    }
  }

  async DeleteAccountTest() {
    return await this.DeleteAccount(this.config.deleteAccountId) && await this.DeleteAccount(this.config.defaultUser.id)  
  }


  /*
  * ----- PRIVATES METHODS
  */

  async DeleteAccount(accountId) {
    const res = await this.axios.get(`/account/delete?accountId=${accountId}`)
    return res && res.status === 200 && res.data.success
  }
}
