import IApi from './IApi'

export default class SiteTest extends IApi {
    constructor(config) {
        super(config)
        console.log('----- Sites Tests -----')
    }

    async start() {
        await this.LoginDefaultUser()
        await super.start()
    }

    async CreateSiteTest() {
        const res = await this.axios.post('/site/create', {
            label: "mon super site",
            phpVersion: "2.1",
            collaborators: []
        })
        if (res && res.status === 200 && res.data.success) {
            this.config.siteCreatedId = res.data.data.id
            return true;
        }
    }

    async ActivateTest() {
        const res = await this.axios.post('/site/activate', {
            siteId: this.config.siteCreatedId
        })
        return (res && res.status === 200 && res.data.success)
    }

    async setRightTest() {
        const res = await this.axios.post('/site/setRight', {
            siteId: this.config.siteCreatedId,
            accountId: this.config.rootUserId,
            permission: "read"
        })
        return (res && res.status === 200 && res.data.success)
    }

    async getMySitesTest() {
        const res = await this.axios.get('/site/my')
        return (res && res.status === 200 && res.data.success && res.data.data.length > 0)
    }
}